## **Deploy network ACLs in AWS with Terraform and GitLab CI (pipelines)**

Full project description at:

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-1-3/

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-2-3/

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-3-3/


### Directory Structure

- In directory **"aws_setup"** is the code to start the Lab setup, from your local machine.  
  - This setup will use GitLab Terraform managed states. In other words, the Terraform states will be 
  stored remotely at GitLab using the http backend. So you will need to setup this in order to
  use those states when updating the security rules.
  - The initialization setup will prepare everything but not open the HTTP port, to do that you
  will have to run the pipeline.
  - The pipelines will have to be manually approved in GitLab
  - Check README.md in **"aws_setup"** directory for more details about the initial setup
  - It uses the AWS credentials from $HOME/.aws default config 
  - When you finish playing don't forget to delete everythin executing `terraform destroy` in
   **"aws_setup"** directory.
 
 
- In **main directory** is the code for deploying/modifying AWS security rules with
 GitLab CI (pipelines), and using GitLab managed Terraform states.  
  - You can view the Terraform states at GitLab -> Operations -> Terraform
  - You will need to create an access key in AWS and configure that key in GitLab.  
  - In this way GitLab will pass the AWS key to Terraform in the pipeline execution.
  - Do this at AWS IAM -> users -> Security Credentials -> Access Keys -> generate access key
  - And in GitLab at project -> settings -> CICD -> variables. There add the 2 variables as
   `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.  

- In **scripts** there are a couple of Python scripts that will help in parsing and generating 
outputs for the pipeline.

- In **test-pipeline** is located a pipeline that will check the status of the services when 
the deploy is finished.
 
####Other notes:
- In order to facilitate ssh connection an ssh key will be generated and installed in
 the instances. Only for development purposes, that key will saved locally as `id_rsa_aws.pem`
 in your PC and in the public server.
 Change its permission to use with `chmod 666 id_rsa_aws.pem`
 For future reference, you can also read it from the states files.

- To connect to the front/public server just run `ssh -i id_rsa_aws.pem ubuntu@public_ip`.

- Likewise to connect to the back/private server just run from the front/public 
server `ssh -i id_rsa_aws.pem ubuntu@back_private_ip`.
