// Setup the remote backend for GitLab managed Terraform states
terraform {
  backend "http" {
  }
}

// Load remote states from for GitLab
data "terraform_remote_state" "cloud_networking" {
  backend = "http"
}
