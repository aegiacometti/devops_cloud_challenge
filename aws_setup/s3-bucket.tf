resource "aws_s3_bucket" "ipspace" {
  bucket = "cloud-networksecurity"
  acl = "public-read"
}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.ipspace.bucket
  key = "my-pic.jpg"
  source = "./web_api/my-pic.jpg"
  storage_class = "ONEZONE_IA"
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.ipspace.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "vpc_only",
    "Statement": [
        {
            "Sid": "GetOnlyFromVPC",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::cloud-networksecurity/my-pic.jpg"
            ],
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": "${aws_instance.front.public_ip}"
                }
            }
        }
    ]
}
POLICY
}
