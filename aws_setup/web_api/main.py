import io
import os

import requests
import urllib3
import uvicorn
from fastapi import FastAPI
from fastapi.responses import HTMLResponse, Response

urllib3.disable_warnings()


def generate_index_file():
    ip_address = os.popen('ip address').read()
    ip_address = ip_address.replace('\n', '<br>')
    ip_address = "<p style=\"font-family: 'Courier New', monospace;\">" + ip_address + "</p> </body>"
    text = """
<title>Networking in Public Clouds - IPspace.net</title>
<body>
<h2>From FastAPI Server</h2>
<h3>It worked !</h3>
<br>
<h2>From AWS S3 bucket (ONLY Front Instance public IP can access)</h2>
<img src='my-pic.jpg'>
<br>
<h2>Server ifconfig</h2>
"""
    text2 = ip_address
    return text + text2


def get_s3_file(url):
    r = requests.get(url)
    file_io = io.BytesIO()
    file_io.write(r.content)
    return file_io


def get_url():
    file = open('s3_bucket_url_my-pic.txt')
    url = file.readline()
    file.close()
    return url.strip()


app = FastAPI()


@app.get("/", response_class=HTMLResponse)
def read_root():
    index_file = generate_index_file()
    return index_file


@app.get("/my-pic.jpg")
def read_root():
    url = get_url()
    io_pic_file = get_s3_file(url)
    return Response(io_pic_file.getvalue())


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80)
