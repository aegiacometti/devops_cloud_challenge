resource "tls_private_key" "ssh-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.ssh-key.public_key_openssh
  tags = {
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

// Save the key file locally
resource "local_file" "key" {
    content     = tls_private_key.ssh-key.private_key_pem
    filename = "./id_rsa_aws.pem"

  provisioner "local-exec" {
    command = "chmod 600 ${self.filename}"
  }
}

data "aws_ami" "aws_linux" {
// most_recent = true
owners = ["099720109477"] # Canonical

  filter {
      name   = "name"
      // values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
      /*
      Force the AMI ID free tier according AWS EC2 Console at 1/2/2021
      */
      values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-20201026"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }
}

resource "aws_instance" "front" {
  ami           = data.aws_ami.aws_linux.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.generated_key.key_name
  subnet_id   = aws_subnet.front.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.infra_sg_public.id, aws_security_group.custom_sg_public.id]

  connection {
      host = self.public_ip
      user = "ubuntu"
      type = "ssh"
      private_key = tls_private_key.ssh-key.private_key_pem
      timeout = "2m"
  }

  // Save the key file
  provisioner "file" {
    content     = tls_private_key.ssh-key.private_key_pem
    destination = "/home/ubuntu/id_rsa_aws.pem"
  }

  // Install and setup web_api server
  provisioner "file" {
    source     = "./web_api/main.py"
    destination = "main.py"
  }

  provisioner "file" {
    source     = "./web_api/fastapi.service"
    destination = "fastapi.service"
  }

  provisioner "file" {
    content = "https://${aws_s3_bucket.ipspace.bucket}.s3.${var.region}.amazonaws.com/${aws_s3_bucket_object.object.key}"
    destination = "/home/ubuntu/s3_bucket_url_my-pic.txt"
  }

  provisioner "remote-exec" {
      inline = [
          "sleep 5",
          "sudo apt update",
          "sleep 5",
          "sudo apt install -y python3-pip",
          "sudo pip3 install fastapi",
          "sudo pip3 install uvicorn[standard]",
          "chmod 600 id_rsa_aws.pem",
          "sudo mv fastapi.service /etc/systemd/system/",
          "sudo systemctl daemon-reload",
          "sudo systemctl enable fastapi.service",
          "sudo shutdown -fr now",
      ]
  }

  tags = {
    Name = "front"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_instance" "back" {
  ami           = data.aws_ami.aws_linux.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.generated_key.key_name
  subnet_id     = aws_subnet.back.id
  associate_public_ip_address = false
  vpc_security_group_ids = [aws_security_group.infra_sg_private.id, aws_security_group.custom_sg_private.id]
  tags = {
    Name = "back"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}
