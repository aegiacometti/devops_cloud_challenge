resource "aws_vpc" "ipspace" {
  cidr_block = "10.1.0.0/16"
  tags = {
    Name = "ipspace"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.ipspace.id
  tags = {
    Name = "ipspace_igw"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.ipspace.id
  tags = {
    Name = "ipspace_public_rt"
    Training = "https://www.ipspace.net/PubCloud/"
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.ipspace.id
  tags = {
    Name = "ipspace_private_rt"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_subnet" "front" {
  vpc_id     = aws_vpc.ipspace.id
  cidr_block = "10.1.1.0/24"
  tags = {
    Name = "front"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_subnet" "back" {
  vpc_id     = aws_vpc.ipspace.id
  cidr_block = "10.1.2.0/24"
  tags = {
    Name = "back"
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.front.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.back.id
  route_table_id = aws_route_table.private.id
}
