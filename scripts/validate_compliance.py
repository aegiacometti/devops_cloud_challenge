import hcl2
import sys
import os

ALLOWED_SECURITY_RULES_FILES = ['sec_rules_custom.tf']
ALLOWED_PROTOCOLS = ['tcp']
allowed_public_ports_init = [80, 443]
allowed_private_ports_init = [80, '161-162', 443, 3000]
ALLOWED_PUBLIC_PORTS = []
ALLOWED_PRIVATE_PORTS = []
# force add this valid segment string to the end of the security group name
VALID_SEGMENTS = ['public', 'private']
LOG_FILENAME = 'compliance.txt'


def main():
    log('Checking compliance:')
    COMPLIANT = True

    terraform_files = os.listdir('./')

    for file in terraform_files:

        if file.endswith('.tf'):

            if file in ALLOWED_SECURITY_RULES_FILES:
                is_compliant, non_compliant_rules = check_compliance(file)

                if is_compliant:
                    log('File \"{}\" is compliant'.format(file))

                else:
                    log('File \"{}\" is NOT compliant!\nNON compliant rule list:'.format(file))
                    COMPLIANT = False
                    for rule in non_compliant_rules:
                        log(str(rule))

            elif is_rouge_security_group_in_file(file):
                log('Rouge Security Group detected in file {}. Aborting!'.format(file))
                COMPLIANT = False

            else:
                # log('{} is compliant'.format(file))
                pass

    if not COMPLIANT:
        sys.exit(1)


def log(text):
    print(text)
    with open(LOG_FILENAME, 'a') as log_file:
        log_file.write(text + "\n")


def check_compliance(file):

    try:
        with open(file, 'r') as f:
            terraform_dict = hcl2.load(f)
    except:
        log('Can\'t open or Invalid file \"{}\"'.format(file))
        sys.exit(1)

    log('Checking protocol and port compliance:')
    log('Allowed security rules files: {}'.format(str(ALLOWED_SECURITY_RULES_FILES)))
    log('Default compliant protocols: {}'.format(str(ALLOWED_PROTOCOLS)))
    generate_allowed_ports_list(allowed_public_ports_init, ALLOWED_PUBLIC_PORTS)
    log('Default compliant ports for \"public\" segment are {}'.format(str(ALLOWED_PUBLIC_PORTS)))
    generate_allowed_ports_list(allowed_private_ports_init, ALLOWED_PRIVATE_PORTS)
    log('Default compliant ports for \"private\" segment are {}'.format(str(ALLOWED_PRIVATE_PORTS)))

#    import pprint
#    pprint.pprint(dict)

    is_compliant = True
    non_compliant_rules = []
    for resource in terraform_dict['resource']:

        for security_group in resource['aws_security_group']:
            for direction in ['ingress', 'egress']:
                if direction in resource['aws_security_group'][security_group].keys():
                    for rule in resource['aws_security_group'][security_group][direction]:
                        if rule['protocol'][0] == 'tcp':
                            ports = port_range(rule['from_port'], rule['to_port'])
                            if 'public' in security_group:
                                if set(ports).issubset(ALLOWED_PUBLIC_PORTS):
                                    continue
                                else:
                                    is_compliant = False
                                    non_compliant_rules.append([security_group, direction, rule])
                            elif 'private' in security_group:
                                if set(ports).issubset(ALLOWED_PRIVATE_PORTS):
                                    continue
                                else:
                                    is_compliant = False
                                    non_compliant_rules.append([security_group, direction, rule])
                            else:
                                is_compliant = False
                                non_compliant_rules.append([security_group, direction,
                                                            'Specify \"public\" or \"private\" segment'])
                        else:
                            is_compliant = False
                            non_compliant_rules.append([security_group, direction, rule])

    return is_compliant, non_compliant_rules


def generate_allowed_ports_list(segment_init, segment_final):
    for port in segment_init:
        if isinstance(port, int):
            segment_final.append(port)
        elif isinstance(port, str):
            ports = port.split('-')
            if len(ports) == 2:
                try:
                    from_port = int(ports[0])
                    to_port = int(ports[1])
                except:
                    log('Invalid allowed port list! Aborting.')
                    sys.exit(1)
                else:
                    if (from_port in range(1, 65535 + 1, 1)) and (to_port in range(1, 65535 + 1, 1)):
                        if from_port <= to_port:
                            for x in range(from_port, to_port + 1, 1):
                                segment_final.append(x)

                        else:
                            log('Invalid allowed port list! Aborting.')
                            sys.exit(1)

                    else:
                        log('Invalid allowed port list! Aborting.')
                        sys.exit(1)

            else:
                log('Invalid allowed port list! Aborting.')
                sys.exit(1)
        else:
            log('Invalid allowed port list! Aborting.')
            sys.exit(1)


def port_range(from_port, to_port):
    if from_port == to_port:
        port = list(from_port)
    else:
        port = list(range(from_port[0], to_port[0] + 1, 1))
    return port


def is_rouge_security_group_in_file(file):
    with open(file, 'r') as f:
        for line in f:
            if 'aws_security_group' in line:
                return True
    return False


if __name__ == '__main__':
    main()
